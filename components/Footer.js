import React from 'react'
import Link from 'next/link'
import styles from '@/styles/Footer.module.css'

const Footer = () => {
  return (
    <div className={styles.footer}>
     <div className={styles.container}>
        <div className={styles.row}>
        <div className={styles.footerCol}>
            <h4 className={styles.colTitle}>A propos</h4>
            <div className={styles.footerUl}>
            <h3 className={styles.footerLi}><a href='/'>Conseil personalisé</a></h3>
            <h3 className={styles.footerLi}><a href='/'>Notre mission</a></h3>
            <h3 className={styles.footerLi}><a href='/'>Nos vendeurs</a></h3>
            <h3 className={styles.footerLi}><a href='/'>Nos avantages</a></h3>
            </div>
        </div>
        <div className={styles.footerCol}>
            <h4 className={styles.colTitle}>Service client</h4>
            <div className={styles.footerUl}>
            <h3 className={styles.footerLi}><a href='/'>Conseil personalisé</a></h3>
            <h3 className={styles.footerLi}><a href='/'>Suivi de commande</a></h3>
            <h3 className={styles.footerLi}><a href='/'>Livraison</a></h3>
            <h3 className={styles.footerLi}><a href='/'>FAQ</a></h3>
            </div>
        </div>
        <div className={styles.footerCol}>
            <h4 className={styles.colTitle}>Catégories</h4>
            <div className={styles.footerUl}>
            <h3 className={styles.footerLi}><a href='/'>Visage</a></h3>
            <h3 className={styles.footerLi}><a href='/'>Corps</a></h3>
            <h3 className={styles.footerLi}><a href='/'>Cheveux</a></h3>
            <h3 className={styles.footerLi}><a href='/'>Bébé</a></h3>
            <h3 className={styles.footerLi}><a href='/'>Homme</a></h3>
            </div>
        </div>
        <div className={styles.footerCol}>
            <h4 className={styles.colTitle}>Contact</h4>
            <div className={styles.footerUl}>
            <div className={styles.social1}>
            <h3 className={styles.footerLi}><a href='/'><img className={styles.socialLinks1}  src={"./images/Marker_icon.svg"}/> 95 Sutton locked France 256815</a></h3>
            <h3 className={styles.footerLi}><a href='/'><img className={styles.socialLinks1} src={"./images/Envelope_icon.svg"}/> info@rosaliss.com</a></h3>
            <h3 className={styles.footerLi}><a href='/'><img className={styles.socialLinks1} src={"./images/Phone_icon.svg"}/> 987-654-3210</a></h3>
              
            </div>
            <div className={styles.social}>
            <a href='/'><img src={"./images/whatsapp.svg"} /></a>
            <a href='/'><img src={"./images/Path_2520.svg"}/></a>
            <a href='/'><img src={"./images/Vector.svg"}/></a>
            </div>
            </div>
        </div>
        </div>
        </div>
        <div className={styles.line}></div>
        <h2 className={styles.footerP}>Tous Les Droits Sont Réservés Pour Ce Site</h2>
        
    </div>
  )
    
}

export default Footer
