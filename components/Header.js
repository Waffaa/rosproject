import React from 'react'
import Image from 'next/image'
import Link from 'next/link'
import styles from '@/styles/Home.module.css'

const Header = () => {
  return (
    <div>
      <img className={styles.banner}/>
      <div className={styles.Header}>
        <div className={styles.top}>
        <nav className={styles.Inner}>
            <Link href="/" className={styles.margin32}>
            Vendre sur Rosaliss
            </Link>
            <Link href="/" className={styles.margin}>
            Programme d'affiliation
            </Link>
            <Link href="/" className={styles.margin20}>
            Blog
            </Link>
            <Link href="/" className={styles.margin20}>
            Livraison
            </Link>
            <Link href="/">
            <img  src={"./images/Frame.png"}/>
            </Link>
        </nav>
        </div>
        <div className={styles.nav} >
         <img className={styles.icon} src={"/images/Logo.svg"}/>
         <div className={styles.search_box}>
           <Image src={"./images/search.svg"} className={styles.span} width="25" height="25"/>
           <input className={styles.input} placeholder="Search.."/>
         </div>
         <div className={styles.ol} >
           <div className={styles.li} ><img src="/images/Combined-Shape.svg"/><a href="#">Panier</a></div>
           <div className={styles.li} ><img src="/images/person.svg"/><a href="#">Compte</a></div>
         </div>  
       </div>
        </div>
    </div>
  )
}

export default Header
