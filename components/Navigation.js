import React from 'react'
import Link from 'next/link'
import styles from '@/styles/Navigation.module.css'

const Navigation = () => {
  return (
    <div className={styles.all}>
      <div className={styles.navigation}>
        <nav className={styles.Inner}>
        <Link href="/" className={styles.link}><img  src={"./images/home.svg"}/></Link>
        <Link href="/" className={styles.link}>Tous les produits</Link>
        <Link href="/" className={styles.link}>Promotion</Link>
        <Link href="/" className={styles.link}>Nouveauté</Link>
        <Link href="/" className={styles.link}>Top catégories</Link>
        <Link href="/" className={styles.link}>Nos marques</Link>
        <Link href="/" className={styles.link}>Mon blog</Link>
        <Link href="/" className={styles.btn}>Upload</Link>
        </nav>
        </div>
    </div>
    
  )
}

export default Navigation
