import React  from 'react' 
{/* , {children} */}
import { useRouter } from 'next/router';
import Head from 'next/head'
import Header from '@/components/Header'
import Navigation from '@/components/Navigation'
import Footer from '@/components/Footer'



const Layout = ({children}) => {

  const router = useRouter();

  const { pathname } = router;
  const noFooter = ['/accueil'];
  const noHeader = ['/accueil'];
  const noNavigation = ['/accueil'];

  return (
    <div>
      <Head>
        <title>Rosaliss -- Affiliate program</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.googleapis.com"/>
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@500&display=swap" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css"/>
      </Head>
      
      {/*
      <Header />
      <Navigation />
       */}
      <main>{children}</main> 
      <div>
      {noHeader.includes(pathname) ? null : <Header />}
        {noNavigation.includes(pathname) ? null : <Navigation />}
        {noFooter.includes(pathname) ? null : <Footer />}
      </div>    
    </div>
  )
}

export default Layout
