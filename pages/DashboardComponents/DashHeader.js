import React from 'react'
import styles from '@/styles/Home.module.css'

const DashHeader = () => {
  return (
  <div className={styles.headcontainer}>
      <div className={styles.headwrapper}>
      <div className={styles.headtitle}>
       <h2 className={styles.dashHeadTitle}>Votre lien de référence</h2>
        <label className={styles.dashFrame}> 
            <h3 className={styles.frameTitle}>https://rosaliss.com/?ref=chouaibblg45<span className={styles.frameSpan}>Copy</span></h3>
        </label> 
      </div>
      </div>
      <div className={styles.profile}>
      <img src="./accueil/user.jpg" className={styles.profileImage}/>
      </div>
    </div>
    
  )
}

export default DashHeader

