import Link from 'next/link'
import React from 'react'
import {AiOutlineMenu, AiOutlineClose,AiOutlineLogout, AiOutlineBarChart, AiOutlineHome} from 'react-icons/ai'
import { useState } from 'react'
import styles from '@/styles/Home.module.css'

const SideNav = () => {
    const [menu , setMenu ] = useState(false)
    const handleNav = () => {
        setMenu(!menu)
    }
  return (
    <>
    <nav className={`${styles.sidenav} ${'fixed w-[200px] h-full shadow-xl bg-[#3E444D]'}`}>
    <div className={`${styles.SideNavContainer} ${'flex'}`}>
      <img href='/' src="./accueil/DashMobLogo.svg" width={"178px"} height={"72px"} className={styles.imgLogo}/>
    <div className={`${styles.nav1} ${'hidden sm:flex'}`}>
      <ul className={`${styles.navItems} ${'hidden sm:flex'}`}>
      <Link href='/' className={`${styles.item} ${'text-center text-xl text-white'}`}>
      <AiOutlineHome size={25} style={{marginBottom:'20px', color:'#fff'}} />Accueil</Link>
      <Link href='/' className={`${styles.item} ${'text-center text-xl text-white'}`} >
      <AiOutlineBarChart size={25} style={{marginBottom:'20px', color:'#fff'}}/>Statistiques</Link>
      <Link href='/' className={styles.logoutBtn}><AiOutlineLogout size={16} style={{marginRight:'3px', marginTop:'4px'}} />Déconnecter</Link>
      </ul>
    </div>
    
    <div onClick={handleNav} className='md:hidden cursor-pointer pl-24'>
     <AiOutlineMenu size={25} style={{color:"white"}} />
    </div>
    </div>
    <div className={menu ? "fixed left-0 top-0 w-[65%] sm:hidden h-screen bg-[#ecf0f3] p-10 ease-in duration-500" : "fixed left-[-100%] top-0 p-10 ease-in duration-500"}>
    <div className='flex w-full items-center justify-end'>
      <div onClick ={handleNav} className='cursor-pointer'>
        <AiOutlineClose size={25}/>
      </div>
    </div>
    <div className='flex-col py-4'>
    <ul>
      <Link href="/">
        <li onClick={() => setMenu(false)}
        className='py-4 cursor-pointer'
        >Accueil</li>
      </Link>
      <Link href="/">
        <li onClick={() => setMenu(false)}
        className='py-4 cursor-pointer'
        >Statiques</li>
      </Link>
      <Link href="/">
        <li onClick={() => setMenu(false)}
        className='py-4 cursor-pointer'
        >Déconnecter</li>
      </Link>
    </ul>
    </div>
    </div>
    </nav>
    </>
  )
}

export default SideNav
