import React, { useState } from 'react'
import styles from '@/styles/Home.module.css'
import { useRouter } from 'next/router'

const LeftNavbar = () => {
{/** */}
const router = useRouter();
const [active, setActive] = useState(false);
{/** */}
  return (
    <>
    <div className={styles.navcontainer}> 
      <div className={styles.wrapper}>
      <ul>
        <li><img src={"/images/home-icon.svg"}/>
          <a href='/'>Accueil</a>  
        </li>
        <li>
        <img src={"/images/statics-icon.svg"}/>
          <a href='/'>Statistiques</a>
        </li>
      </ul>
      </div>
      <a href='/' className={styles.btn4}><img src={"/images/Logout_icon.svg"}/>Déconnecter</a>
    </div>
    </>
   
  )
}

export default LeftNavbar
