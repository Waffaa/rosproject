import React from 'react'
import styles from '@/styles/Home.module.css'

const AffPContent = () => {
  return (
    <div className={styles.contentContainer}>
      <div className={styles.contentWrapper}>
       <div className={styles.tabs}>
         <div className={styles.categories}>
         <div className={styles.imgframe}>
          <img src="./accueil/accueilProfile.svg"/>
         </div>
         <div className={styles.textframe}>
          <h2>32565</h2>
          <p style={{fontSize:"14px"}}>Conversions</p>
         </div>
         </div>
        </div>

        <div className={styles.tabs}>
         <div className={styles.categories}>
         <div className={styles.imgframe}>
          <img src="./accueil/Bag.svg"/>
         </div>
         <div className={styles.textframe}>
          <h2>35679</h2>
          <p style={{fontSize:"14px"}}>Clicks</p>
         </div>
         </div>
        </div>

        <div className={styles.tabs}>
         <div className={styles.categories}>
         <div className={styles.imgframe}>
          <img src="./accueil/Wallet.svg"/>
         </div>
         <div className={styles.textframe}>
          <h2>20</h2>
          <p style={{fontSize:"14px"}}>Commission</p>
         </div>
         </div>
        </div>

        <div className={styles.tabs}>
         <div className={styles.categories}>
         <div className={styles.imgframe}>
          <img src="./accueil/Wallet.svg"/>
         </div>
         <div className={styles.textframe}>
          <h2>DZ 14,966</h2>
          <p style={{fontSize:"10px"}}>Montant des commissions</p>
         </div>
         </div>
        </div>
      </div>
      {/* chart started */}
      <div className={styles.stat}>
        <div className={styles.statCard}>
        <div className={styles.statContent}>
        <div style={{fontSize:"16px", fontWeight:"700"}}>Statiques des commandes</div>
        <div className={styles.cardTabs}>
          <div className={styles.cardTab}>
          <h2 style={{fontSize:"16px", fontWeight:"700"}}>325</h2>
          <p style={{fontSize:"14px", fontWeight:"400"}}>Clicks</p>
        </div>
        <div className={styles.cardTab}>
          <h2 style={{fontSize:"16px", fontWeight:"700"}}>325</h2>
          <p style={{fontSize:"14px", fontWeight:"400"}}>Conversions</p>
        </div>
        </div>
        </div>
        <div className={styles.chart}>
        chart here
        </div>
      </div>
      <div className={styles.statCard2}>
        
      </div>
      </div>
      
    </div>
      
  )
}

export default AffPContent
