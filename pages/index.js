import Image from 'next/image'
import { Inter } from '@next/font/google'
import styles from '@/styles/Home.module.css'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
    <div className={styles.hero}>
    <div className={styles.heroImgMobile}>
        <img src="./images/heroImgMob.svg" />
    </div>
      <div className={styles.block}>
      <div className={styles.breadcrumb}>
        <a href='/' style={{fontWeight:"500", fontSize:"16px", color:"#636973", marginRight:"13.17px"}}> Accueil</a><img src="./images/Stroke.svg" /><h3 style={{fontWeight:"500", fontSize:"16px", color:"#D81B69", marginLeft:"13.17px"}}>Programme d'affiliation</h3>
      </div>
      <div className={styles.heroContent}>
        <h1 className={styles.heroHeadline}>Rejoignez  notre <span style={{color:"#D81B69"}}>programme d'affiliation</span> </h1>
        <p className={styles.heroText}>Après des mois de gestation nu dans un milieu aquatique, votre bébé se retrouve à porter des couches toute la journée. Ces dernières vont finir par irriter sa peau à cause du frottement mais pas uniquement. L’humidité et les bactéries contenues dans les couches vont causer une réaction de la peau : l’érythème fessier des bébés.</p>
        <div className={styles.heroButtons}>
        <a href="/signup" className={styles.heroStaticsBtn}>Devenir un affilié <img src={"./images/primary_btn.svg"}/></a>
        <a href='/' className={styles.heroBtn}>Connectez-vous en tant qu'affilié <img src={"./images/secondary_btn.svg"}/></a>
        </div>
      </div>
      </div>
      <div className={styles.heroImgWeb}>
        <img src="./images/heroImg.svg" />
      </div>
      
    </div>
    <div className={styles.affiliateProgram}>
      <div className={styles.content}>
        <h3 className={styles.headline}>Le programme d'affiliation</h3>
        <h4 className={styles.text}>Après des mois de gestation nu dans un milieu aquatique, votre bébé se retrouve à porter des couches toute la journée. Ces dernières vont finir par irriter sa peau à cause du frottement mais pas uniquement. L’humidité et les bactéries contenues dans les couches vont causer une réaction de la peau : l’érythème fessier des bébés.</h4>
        <div className={styles.cards}>
          <div className={styles.card}>
            <img src={"./images/calendar.svg"} className={styles.cardImg}/>
            <h3 className={styles.cardTitle}>Cookies</h3>
            <h4 className={styles.cardContent}>Lorem ipsum dolor sit amet, consectetur.</h4>
          </div>
          <div className={styles.card}>
            <img src={"./images/inscription.svg"} className={styles.cardImg}/>
            <h3 className={styles.cardTitle}>Inscription gratuite</h3>
            <h4 className={styles.cardContent}>Lorem ipsum dolor sit amet, consectetur.</h4>
          </div>
          <div className={styles.card}>
            <img src={"./images/Money.svg"} className={styles.cardImg}/>
            <h3 className={styles.cardTitle}>Paiements pro et équipe</h3>
            <h4 className={styles.cardContent}>Lorem ipsum dolor sit amet, consectetur.</h4>
          </div>
        </div>
      </div>
    </div>
    <div className={styles.statics}>
      <div className={styles.staticsHeadline}>
        Un regard sur
        <br /> Rosaliss en chiffres
      </div>
      <div className={styles.staticsFrame}>
        <div className={styles.static}>
        <h3 className={styles.contentTitle}>140K+</h3>
          <p className={styles.staticContent}>Lorem ipsum dolor sit amet, consectetur.</p>
        </div>
        <div className={styles.static}>
        <h3 className={styles.contentTitle}>1M+</h3>
          <p className={styles.staticContent}>Lorem ipsum dolor sit amet, consectetur.</p>
        </div>
        <div className={styles.static}>
          <h3 className={styles.contentTitle}>50K+</h3>
          <p className={styles.staticContent}>Lorem ipsum dolor sit amet, consectetur.</p>
        </div>
        <div className={styles.static}>
        <h3 className={styles.contentTitle}>150+</h3>
          <p className={styles.staticContent}>Lorem ipsum dolor sit amet, consectetur.</p>
        </div>
      </div>
      <a href="/SignUp" className={styles.staticsBtn}>
        Devenir un affilié
        <img src={"./images/primary_btn.svg"}/>
      </a>
    </div>
    </>
  )
}
