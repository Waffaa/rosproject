import React from 'react'
import styles from '@/styles/Home.module.css'
import Link from 'next/link'
import Image from 'next/image'

const SignUp = () => {
  return (
    <div>
    <div className={styles.registerLogo}>
    <img src="./images/registerLogo.svg" />
    </div>
      <div className={styles.register}>
        <div className={styles.registerLeft}>
        <div className={styles.registerBreadcrumb}>
        <a href='/' style={{fontWeight:"500", fontSize:"16px", color:"#636973", marginRight:"13.17px"}}> Accueil</a><img src="./images/Stroke.svg" /><h3 style={{fontWeight:"500", fontSize:"16px", color:"#D81B69", marginLeft:"13.17px"}}>Devenir un affilié</h3>
        </div>
        <h1 className={styles.registerHeadline}>Bienvenue chez Rosaliss Affiliés</h1>
        <div className={styles.Tabs}>
          <div className={`${styles.registerTab} ${styles.active}`}><a href="/signin" className={`${styles.registerLink} ${styles.activeLink}`}>Créer un compte</a> </div>
          <div className={styles.connectTab}><a href="/signin" className={styles.connectLink}>Connexion</a></div>
        </div>
        <p className={styles.registerText}>Inscrivez-vous pour devenir notre affilié et gagner des récompenses. Après votre inscription, vous aurez accès à votre lien de parrainage personnalisé et à votre propre tableau de bord.</p>
        <div className={styles.registerForm}>
         <form className={styles.registerInputs}>
         <div className={styles.registerInputText}>
          <h3>Nom</h3>
          <input placeholder='Nom' className={styles.registerInputContent}/>
         </div>
         <div className={styles.registerInputText}>
          <h3>Prénom</h3>
          <input placeholder='Prénom' className={styles.registerInputContent}/>
         </div>
         <div className={styles.registerInputText}>
          <h3>Adresse e-mail</h3>
          <input placeholder='Adresse e-mail' className={styles.registerInputContent}/>
         </div>
         <div className={styles.registerInputText}>
           <h3>Mot de passe</h3>
           <input placeholder="Mot de passe" className={styles.registerInputContent}/>
           <Image src={"./images/eye.svg"} className={styles.eye} width="20" height="16"/>
         </div>
         </form> 
         <Link href={'/'} className={styles.registerBtn}>Créer un compte</Link>
        </div>
        </div>
        <div className={styles.registerImg}>
            <img src="./images/affiliate02.svg" />
        </div>
      </div>
    </div>
  )
}

export default SignUp
